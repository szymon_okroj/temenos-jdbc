package mysql;

import java.sql.*;


public class JdbcExample {
    private static final String URL = "jdbc:mysql://localhost/temenos?serverTimezone=UTC&useSSL=false";
    private static final String USER = "postgres";
    private static final String PASS = "postgres";

    public static void main(String[] args) {
        String sql = "select id, name, surname from employee limit 10";

        try (Connection connection = DriverManager.getConnection(URL, USER, PASS)){
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                int id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                System.out.println("id: " + id + ", name: " + name);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
